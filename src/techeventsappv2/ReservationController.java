/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package techeventsappv2;

import entities.Reservation;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import services.ReservationService;
import utils.connectionDB;

/**
 * FXML Controller class
 *
 * @author Yassine
 */
public class ReservationController implements Initializable {
    
    @FXML 
    TextField type;
    @FXML 
    TextField nbrplaces;
    @FXML
    TextField seat;
    @FXML 
    Text totalText;
    connectionDB c = connectionDB.getInstance();
    Connection con = c.getConnection();

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
       nbrplaces.textProperty().addListener(new ChangeListener<String>() {
    public void changed(ObservableValue<? extends String> observable,
            String oldValue, String newValue) {
      //requete pour avoir le prix
      try{
         String requete2 ="select prix from event where id = 1";
         PreparedStatement st2 = con.prepareStatement(requete2);
         ResultSet rsprix =st2.executeQuery();
         rsprix.next();
         double prixevent = rsprix.getInt(1);
         int prixevent2 = (int) prixevent;
         int res = prixevent2*Integer.parseInt(newValue);
          totalText.setText("PRIX TOTAL : "+res+" TND");
      }catch(SQLException ex){
        System.out.println(ex.getMessage());
      }
      //requete bch nekhou le prix
    }


       });
    }  
    
    @FXML 
    private void ReservationAction(ActionEvent event) throws SQLException {
       String t = type.getText();
       int nbp = Integer.parseInt(nbrplaces.getText());
       String s =  seat.getText();
       Reservation r = new Reservation(type.getText(),s,nbp,1,1);
       ReservationService rs = new ReservationService();
       rs.AjouterReservation(r);
    }
    @FXML
   private void PanierBtnAction(ActionEvent event) throws SQLException, IOException {
   Parent home_page_parent = FXMLLoader.load(getClass().getResource("panier.fxml"));
   Scene home_page_scene = new Scene(home_page_parent);
   Stage appStage = (Stage)((Node)event.getSource()).getScene().getWindow();
   appStage.setScene(home_page_scene);
   appStage.show();
   }
     
   //Rechercher dans l'historique
}
